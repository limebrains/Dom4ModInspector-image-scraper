Dom 4 mod scrapper
=================

Simple scrapper for https://larzm42.github.io/dom4inspector/ best used as an compoment for any IRC bot. Made for *fun* and educational purposes. 

Functional Requirements
---
* Installable by pip
* Screenshots and stores images and data of items, spells and units to get them "offline"
* Can compare two or more items by requested criteria (paths/stats/nations/percentage of dodges/average damage etc.)

Used third-party modules
---
* Obviously dom4modinspector by larzm
* Selenium with PhantomJs

Planned expansion
---
* Calculating percentage of dodges, sucessful attacks and damage when comparing two units
* Usage of direct game data instead of scrapping the web page
* Support of mods

Example usage
-----
```
    >.unit air elemental
    <screenshot of air elemental from dom4mod inspector>>.stats unit air elemental
    Air Elemental - Unit
    HP:14 (size 6) Prot: 0, MR: 18, Morale: 50 (don't rout)
    Strenght: 15, Attack: 16, Defence: 16, Precision: 5, Enc: 0, Move: 3 / 30
    Resist poison: 25 Resist shock: 15, Storm power: 4, Blind fighter: 1
    Wounded shape - Air Elemental (size 5)
    <extra stats>
    
    >.stats HP air elemental
    HP: 14 (size 6)
    
    >.compare paths augur and theurg
    Augur (65 gold): 1S 1F R9
    Theurg (215 gold): 1A 1W 2S H2 R13
```